package com.practi.bookapp.configuration

import com.mongodb.reactivestreams.client.MongoClients
import com.practi.bookapp.book.domain.Book
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.event.EventListener
import org.springframework.data.mongodb.config.AbstractReactiveMongoConfiguration
import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import org.springframework.data.mongodb.core.index.MongoPersistentEntityIndexResolver
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories


@Configuration
@EnableReactiveMongoRepositories(basePackages = ["com.practi.bookapp.book.repository"])
internal class MongoConfiguration(
        @Value("\${spring.data.mongodb.uri}") private val connectionString: String
) : AbstractReactiveMongoConfiguration() {

    override fun reactiveMongoClient() = MongoClients.create(connectionString)!!

    override fun getDatabaseName() = "bookapp"

    @Bean
    override fun reactiveMongoTemplate() = ReactiveMongoTemplate(reactiveMongoClient(), databaseName)

    @EventListener(ApplicationReadyEvent::class)
    fun initIndicesAfterStartup() {
        val indexOps = reactiveMongoTemplate().indexOps(Book::class.java)

        val resolver = MongoPersistentEntityIndexResolver(mappingMongoConverter().mappingContext)
        resolver.resolveIndexFor(Book::class.java).forEach { indexOps.ensureIndex(it) }
    }

    override fun autoIndexCreation() = false
}