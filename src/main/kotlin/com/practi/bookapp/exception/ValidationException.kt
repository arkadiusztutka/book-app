package com.practi.bookapp.exception

import javax.validation.ConstraintViolation

internal data class MissingRequirementParameter(val parameterName: String) : IllegalStateException("Missing required parameter: $parameterName")

internal data class InvalidIdException(val id: String): IllegalArgumentException("Invalid book id: $id")

internal data class BookIdNotFoundException(val id: String): IllegalArgumentException("Given book with id $id not exists")

internal data class ValidationException(val violations: Set<ConstraintViolation<out Any>>): RuntimeException()

internal data class IsbnNumberAlreadyExistsException(val isbnNumber: String) : IllegalArgumentException("ISBN number $isbnNumber already exists")