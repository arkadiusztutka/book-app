package com.practi.bookapp.exception.handler

import com.fasterxml.jackson.databind.ObjectMapper
import com.practi.bookapp.exception.BookIdNotFoundException
import com.practi.bookapp.exception.InvalidIdException
import com.practi.bookapp.exception.IsbnNumberAlreadyExistsException
import com.practi.bookapp.exception.MissingRequirementParameter
import com.practi.bookapp.exception.ValidationException
import org.springframework.boot.web.reactive.error.ErrorWebExceptionHandler
import org.springframework.core.annotation.Order
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType.APPLICATION_JSON
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Component
import org.springframework.web.server.ServerWebExchange
import org.springframework.web.server.ServerWebInputException
import reactor.core.publisher.Mono
import reactor.core.publisher.Mono.just

@Component
@Order(-2)
internal class GlobalErrorWebExceptionHandler(
        val objectMapper: ObjectMapper
) : ErrorWebExceptionHandler {

    override fun handle(exchange: ServerWebExchange, ex: Throwable): Mono<Void> {
        val response = when (ex) {
            is MissingRequirementParameter -> handle(ex)
            is InvalidIdException -> handle(ex)
            is ValidationException -> handle(ex)
            is ServerWebInputException -> handle(ex)
            is BookIdNotFoundException -> handle(ex)
            is IsbnNumberAlreadyExistsException -> handle(ex)
            else -> handle(ex)
        }

        exchange.response.headers.contentType = APPLICATION_JSON
        exchange.response.statusCode = response.statusCode
        val bytes = objectMapper.writeValueAsBytes(response.body)
        val buffer = exchange.response.bufferFactory().wrap(bytes)
        return exchange.response.writeWith(just(buffer))
    }

    private fun handle(exception: MissingRequirementParameter) = ResponseEntity(
            ServerError(message = exception.message),
            HttpStatus.BAD_REQUEST)

    private fun handle(exception: InvalidIdException) = ResponseEntity(
            ServerError(message = exception.message),
            HttpStatus.BAD_REQUEST)

    private fun handle(exception: BookIdNotFoundException) = ResponseEntity(
            ServerError(message = exception.message),
            HttpStatus.BAD_REQUEST)

    private fun handle(exception: IsbnNumberAlreadyExistsException) = ResponseEntity(
            ServerError(message = exception.message),
            HttpStatus.BAD_REQUEST)

    private fun handle(exception: ValidationException): ResponseEntity<Errors> {
        val errors = exception.violations.map { ServerError(it.message) }.toList()
        return ResponseEntity(
                Errors(errors),
                HttpStatus.BAD_REQUEST)
    }

    private fun handle(exception: ServerWebInputException) = ResponseEntity(
                ServerError(message = exception.reason + ". Please check if your request body is correct"),
                HttpStatus.BAD_REQUEST)

    private fun handle(exception: Throwable) = ResponseEntity(
            ServerError(message = exception.message),
            HttpStatus.INTERNAL_SERVER_ERROR)

}

internal data class ServerError(val message: String?)
internal data class Errors(val errors: List<ServerError>)
