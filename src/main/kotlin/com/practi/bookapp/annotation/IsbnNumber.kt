package com.practi.bookapp.annotation

import javax.validation.Constraint
import javax.validation.Payload
import kotlin.reflect.KClass


@MustBeDocumented
@Constraint(validatedBy = [IsbnNumberValidator::class])
@Target(AnnotationTarget.FIELD)
@Retention(AnnotationRetention.RUNTIME)
annotation class IsbnNumber(
        val message: String = "Invalid ISBN number. It should be in format 000000000 or 0000000000000",
        val groups: Array<KClass<*>> = [],
        val payload: Array<KClass<out Payload>> = [])