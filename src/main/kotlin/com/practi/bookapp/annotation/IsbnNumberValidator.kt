package com.practi.bookapp.annotation

import javax.validation.ConstraintValidator
import javax.validation.ConstraintValidatorContext

internal class IsbnNumberValidator : ConstraintValidator<IsbnNumber, String> {

    override fun isValid(value: String?, context: ConstraintValidatorContext?): Boolean {
        if (value == null) {
            return true
        }
        if (value.length > 13) {
            return false
        }
        if (value.toLongOrNull() == null) {
            return false
        }
        if (value.length == 10) {
            return validateIsbn10(value)
        }
        if (value.length == 13) {
            return validateIsbn13(value)
        }
        return false
    }

    private fun validateIsbn10(value: String): Boolean {
        val checksum = (1..9).fold(0, { v, i -> i * value[i - 1].intValue() + v })
        return checksum % 11 == value[9].intValue()
    }

    private fun validateIsbn13(value: String): Boolean {
        val checksum = (1..12).fold(0, { v, i ->
            val w = if (i % 2 == 1) 1 else 3
            w * value[i - 1].intValue() + v
        })
        if (checksum % 10 == 0) {
            return 0 == value[12].intValue()
        }
        return 10 - (checksum % 10) == value[12].intValue()
    }

    private fun Char.intValue() = this.toString().toInt()
}