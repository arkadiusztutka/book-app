package com.practi.bookapp.book.repository

import com.mongodb.client.result.UpdateResult
import com.practi.bookapp.book.domain.Book
import com.practi.bookapp.book.domain.Comment
import org.bson.types.ObjectId
import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Query
import org.springframework.data.mongodb.core.query.Update
import org.springframework.stereotype.Component
import reactor.core.publisher.Mono

internal interface BookCustomRepository {
    fun addCommentToBook(bookId: ObjectId, comment: Comment): Mono<UpdateResult>
}

@Component
internal class BookCustomRepositoryImpl(
        private val mongoTemplate: ReactiveMongoTemplate
): BookCustomRepository {

    override fun addCommentToBook(bookId: ObjectId, comment: Comment): Mono<UpdateResult> {
        val update = Update()
        update.addToSet("comments", comment)
        val query = Query(Criteria.where("_id").`is`(bookId))
        return mongoTemplate.updateFirst(query, update, Book::class.java)
    }

}