package com.practi.bookapp.book.repository

import com.practi.bookapp.book.domain.Book
import org.bson.types.ObjectId
import org.springframework.data.domain.PageRequest
import org.springframework.data.mongodb.repository.Query
import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import org.springframework.stereotype.Repository
import reactor.core.publisher.Flux

@Repository
internal interface BookRepository : ReactiveMongoRepository<Book, ObjectId>, BookCustomRepository {

    @Query(value = "{}", fields = "{'comments': { '\$slice': -5 } }")
    fun findAllWithLastFiveComments(pageRequest: PageRequest): Flux<Book>
}