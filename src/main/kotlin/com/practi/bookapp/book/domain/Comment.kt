package com.practi.bookapp.book.domain

internal data class Comment(
        val comment: String
)