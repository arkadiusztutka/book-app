package com.practi.bookapp.book.domain

import org.bson.types.ObjectId
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "book")
internal data class Book(
        @Id val id: ObjectId = ObjectId(),
        val title: String,
        val author: String? = null,
        @Indexed(unique=true, sparse = true) val isbnNumber: String? = null,
        val numberOfPages: Int? = null,
        val rating: Int? = null,
        val comments: List<Comment> = emptyList()
)