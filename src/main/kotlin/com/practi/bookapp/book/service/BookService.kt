package com.practi.bookapp.book.service

import com.practi.bookapp.book.domain.Book
import com.practi.bookapp.book.domain.Comment
import com.practi.bookapp.book.repository.BookRepository
import com.practi.bookapp.book.request.AddCommentRequest
import com.practi.bookapp.book.request.BookCreateRequest
import com.practi.bookapp.book.request.BookSearchRequest
import com.practi.bookapp.book.request.BookUpdateRequest
import com.practi.bookapp.exception.BookIdNotFoundException
import com.practi.bookapp.exception.IsbnNumberAlreadyExistsException
import mu.KLogging
import org.bson.types.ObjectId
import org.springframework.dao.DuplicateKeyException
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.core.scheduler.Schedulers

internal interface BookService {
    fun create(createRequest: BookCreateRequest): Mono<Book>
    fun search(searchRequest: BookSearchRequest): Flux<Book>
    fun update(updateRequest: BookUpdateRequest): Mono<Book>
    fun delete(id: ObjectId): Mono<Void>
    fun addComment(request: AddCommentRequest): Mono<Void>
}

@Service
internal class BookServiceImpl(
        private val bookRepository: BookRepository
) : BookService {

    private companion object : KLogging()

    private val scheduler = Schedulers.newElastic("bookapp")

    override fun search(searchRequest: BookSearchRequest) =
            bookRepository.findAllWithLastFiveComments(PageRequest.of(searchRequest.page, searchRequest.limit))
                    .subscribeOn(scheduler)

    override fun create(createRequest: BookCreateRequest): Mono<Book> {
        return bookRepository
                .save(createRequest.toBook())
                .doOnError(DuplicateKeyException::class.java) {
                    logger.error { "When creating found book in db with isbn number ${createRequest.isbnNumber}" }
                    throw IsbnNumberAlreadyExistsException(createRequest.isbnNumber!!)
                }
                .subscribeOn(scheduler)
    }

    override fun update(updateRequest: BookUpdateRequest): Mono<Book> {
        return bookRepository
                .existsById(updateRequest.id)
                .flatMap {
                    if (it) {
                        bookRepository.save(updateRequest.toBook())
                                .doOnError(DuplicateKeyException::class.java) {
                                    logger.error { "When updating found book in db with isbn number ${updateRequest.isbnNumber}" }
                                    throw IsbnNumberAlreadyExistsException(updateRequest.isbnNumber!!)
                                }
                    } else {
                        logger.error { "Update for book id $updateRequest.id wasn't performed, because book id not exists" }
                        throw BookIdNotFoundException(updateRequest.id.toHexString())
                    }
                }
                .subscribeOn(scheduler)
    }

    override fun delete(id: ObjectId): Mono<Void> {
        return bookRepository
                .existsById(id)
                .flatMap {
                    if (it) {
                        bookRepository.deleteById(id)
                    } else {
                        logger.error { "Delete for book id $id wasn't performed, because book id not exists" }
                        throw BookIdNotFoundException(id.toHexString())
                    }
                }
                .subscribeOn(scheduler)
    }

    override fun addComment(request: AddCommentRequest): Mono<Void> {
        return bookRepository.addCommentToBook(request.bookId, request.toComment())
                .flatMap {
                    if (it.matchedCount > 0) {
                        Mono.empty<Void>()
                    } else {
                        logger.error { "Adding comment tp book with id ${request.bookId} wasn't performed, because book id not exists" }
                        throw BookIdNotFoundException(request.bookId.toHexString())
                    }
                }
                .subscribeOn(scheduler)
    }

    private fun BookCreateRequest.toBook() = Book(
            title = title,
            author = author,
            isbnNumber = isbnNumber,
            numberOfPages = numberOfPages,
            rating = rating)

    private fun BookUpdateRequest.toBook() = Book(
            id = id,
            title = title,
            author = author,
            isbnNumber = isbnNumber,
            numberOfPages = numberOfPages,
            rating = rating)

    private fun AddCommentRequest.toComment() = Comment(comment)

}