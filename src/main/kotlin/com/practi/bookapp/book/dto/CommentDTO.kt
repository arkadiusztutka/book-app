package com.practi.bookapp.book.dto

internal data class CommentDTO(
        val comment: String
)