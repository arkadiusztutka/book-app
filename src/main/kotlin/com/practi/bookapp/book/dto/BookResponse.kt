package com.practi.bookapp.book.dto

internal class BookResponse(
        val books: List<BookDTO>
)