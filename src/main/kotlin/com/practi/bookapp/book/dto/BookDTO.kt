package com.practi.bookapp.book.dto

internal data class BookDTO(
        val id: String,
        val title: String,
        val author: String?,
        val isbnNumber: String?,
        val numberOfPages: Int?,
        val rating: Int?,
        val comments: List<CommentDTO>
)