package com.practi.bookapp.book.request.dto

import com.practi.bookapp.annotation.IsbnNumber
import javax.validation.constraints.DecimalMax
import javax.validation.constraints.DecimalMin
import javax.validation.constraints.NotBlank
import javax.validation.constraints.Positive

internal data class BookUpdateRequestDTO(
        @field:NotBlank(message = "Title cannot be blank or null")
        val title: String,
        val author: String?,
        @IsbnNumber
        val isbnNumber: String?,
        @field:Positive(message = "Number of pages cannot be negative value")
        val numberOfPages: Int?,
        @field:DecimalMin("1", message = "Minimal value for rating is 1")
        @field:DecimalMax("5", message = "Maximum value for rating is 5")
        val rating: Int?
)