package com.practi.bookapp.book.request

internal data class BookCreateRequest(
        val title: String,
        val author: String? = null,
        val isbnNumber: String? = null,
        val numberOfPages: Int? = null,
        val rating: Int? = null
)