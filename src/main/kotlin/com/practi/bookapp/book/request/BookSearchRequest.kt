package com.practi.bookapp.book.request

internal data class BookSearchRequest(
        val page: Int,
        val limit: Int
)