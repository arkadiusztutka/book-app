package com.practi.bookapp.book.request.dto

import javax.validation.constraints.DecimalMax
import javax.validation.constraints.DecimalMin

internal data class BookSearchRequestDTO(
        @field:DecimalMin("0", message = "Minimal value for page is 0")
        val page: Int,
        @field:DecimalMin("1", message = "Minimal value for limit is 1")
        @field:DecimalMax("100", message = "Maximum value for limit is 100")
        val limit: Int
)