package com.practi.bookapp.book.request.dto

import javax.validation.constraints.NotBlank
import javax.validation.constraints.Size

internal data class AddCommentRequestDTO(
        @field:NotBlank(message = "Comment cannot be blank")
        @field:Size(max = 3000, message = "Comment cannot have more than 3000 characters")
        val comment: String
)