package com.practi.bookapp.book.request

import org.bson.types.ObjectId

internal data class BookUpdateRequest(
        val id: ObjectId,
        val title: String,
        val author: String? = null,
        val isbnNumber: String? = null,
        val numberOfPages: Int? = null,
        val rating: Int? = null
)