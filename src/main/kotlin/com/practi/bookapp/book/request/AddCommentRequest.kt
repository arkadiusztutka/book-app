package com.practi.bookapp.book.request

import org.bson.types.ObjectId

internal data class AddCommentRequest(
        val bookId: ObjectId,
        val comment: String
)