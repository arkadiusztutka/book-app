package com.practi.bookapp.book.handler

import com.practi.bookapp.book.domain.Book
import com.practi.bookapp.book.dto.BookDTO
import com.practi.bookapp.book.dto.BookResponse
import com.practi.bookapp.book.dto.CommentDTO
import com.practi.bookapp.book.request.BookSearchRequest
import com.practi.bookapp.book.request.dto.BookSearchRequestDTO
import com.practi.bookapp.book.service.BookService
import com.practi.bookapp.exception.MissingRequirementParameter
import com.practi.bookapp.exception.ValidationException
import mu.KLogging
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.server.HandlerFunction
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono
import javax.validation.Validator

@Component
internal class SearchBookHandler(
        private val bookService: BookService,
        private val validator: Validator
) : HandlerFunction<ServerResponse> {

    private companion object : KLogging()

    override fun handle(request: ServerRequest): Mono<ServerResponse> {
        return request.toSearchRequest()
                .toMono()
                .map { it.toBookSearchRequest() }
                .doOnNext {
                    val constraintViolations = validator.validate(it)
                    if(constraintViolations.isNotEmpty()) {
                        logger.error { "Validation errors when searching books: ${constraintViolations.map { v -> v.message }}" }
                        throw ValidationException(constraintViolations)
                    }
                }
                .flatMapMany { bookService.search(it) }
                .map { it.toDTO() }
                .collectList()
                .map { BookResponse(it) }
                .flatMap {
                    ServerResponse.ok().bodyValue(it)
                }
    }

    private fun ServerRequest.toSearchRequest(): BookSearchRequestDTO {
        return BookSearchRequestDTO(
                page = queryParam(BookSearchRequestDTO::page.name)
                        .map { it.toInt() }
                        .orElseThrow { MissingRequirementParameter(BookSearchRequestDTO::page.name) },
                limit = queryParam(BookSearchRequestDTO::limit.name)
                        .map { it.toInt() }
                        .orElseThrow { MissingRequirementParameter(BookSearchRequestDTO::limit.name) }
        )
    }

    fun Book.toDTO() = BookDTO(
            id = id.toHexString(),
            title = title,
            author = author,
            isbnNumber = isbnNumber,
            numberOfPages = numberOfPages,
            rating = rating,
            comments = comments.map { CommentDTO(it.comment) }
    )

    private fun BookSearchRequestDTO.toBookSearchRequest() = BookSearchRequest(page = page, limit = limit)
}