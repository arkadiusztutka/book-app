package com.practi.bookapp.book.handler

import com.practi.bookapp.book.service.BookService
import com.practi.bookapp.exception.InvalidIdException
import mu.KLogging
import org.bson.types.ObjectId
import org.bson.types.ObjectId.isValid
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.server.HandlerFunction
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.ServerResponse.ok
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono

@Component
internal class DeleteBookHandler(
        private val bookService: BookService
) : HandlerFunction<ServerResponse> {

    private companion object : KLogging()

    override fun handle(request: ServerRequest): Mono<ServerResponse> {
        return request
                .toMono()
                .map { it.pathVariable("id") }
                .doOnNext {
                    if(!isValid(it)) {
                        logger.error { "Book with id $it is not valid and can't perform deleting book" }
                        throw InvalidIdException(it)
                    }
                }
                .map { ObjectId(it) }
                .flatMap { bookService.delete(it) }
                .then(ok().build())
    }

}