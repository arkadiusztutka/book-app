package com.practi.bookapp.book.handler

import com.practi.bookapp.book.request.BookCreateRequest
import com.practi.bookapp.book.request.dto.BookCreateRequestDTO
import com.practi.bookapp.book.service.BookService
import com.practi.bookapp.exception.ValidationException
import mu.KLogging
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.server.HandlerFunction
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.ServerResponse.ok
import reactor.core.publisher.Mono
import javax.validation.Validator

@Component
internal class CreateBookHandler(
        private val bookService: BookService,
        private val validator: Validator
) : HandlerFunction<ServerResponse> {

    private companion object : KLogging()

    override fun handle(request: ServerRequest): Mono<ServerResponse> {
        return request
                .bodyToMono(BookCreateRequestDTO::class.java)
                .doOnNext {
                    val constraintViolations = validator.validate(it)
                    if(constraintViolations.isNotEmpty()) {
                        logger.error { "Validation errors when creating book: ${constraintViolations.map { v -> v.message }}" }
                        throw ValidationException(constraintViolations)
                    }
                }
                .map { it.toBookCreateRequest() }
                .flatMap { bookService.create(it) }
                .then(ok().build())
    }

    private fun BookCreateRequestDTO.toBookCreateRequest() =
            BookCreateRequest(
                    title = title,
                    author = author,
                    isbnNumber = isbnNumber,
                    numberOfPages = numberOfPages,
                    rating = rating)

}