package com.practi.bookapp.book.handler

import com.practi.bookapp.book.request.BookUpdateRequest
import com.practi.bookapp.book.request.dto.BookUpdateRequestDTO
import com.practi.bookapp.book.service.BookService
import com.practi.bookapp.exception.InvalidIdException
import com.practi.bookapp.exception.ValidationException
import mu.KLogging
import org.bson.types.ObjectId
import org.bson.types.ObjectId.isValid
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.server.HandlerFunction
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.ServerResponse.ok
import reactor.core.publisher.Mono
import javax.validation.Validator

@Component
internal class UpdateBookHandler(
        private val bookService: BookService,
        private val validator: Validator
) : HandlerFunction<ServerResponse> {

    private companion object : KLogging()


    override fun handle(request: ServerRequest): Mono<ServerResponse> {
        return request
                .bodyToMono(BookUpdateRequestDTO::class.java)
                .doOnNext {
                    val id = request.pathVariable("id")
                    if(!isValid(id)) {
                        logger.error { "Book with id $id is not valid and can't perform updating book" }
                        throw InvalidIdException(id)
                    }
                    val constraintViolations = validator.validate(it)
                    if(constraintViolations.isNotEmpty()) {
                        logger.error { "Validation errors when updating book: ${constraintViolations.map { v -> v.message }}" }
                        throw ValidationException(constraintViolations)
                    }
                }
                .map { it.toBookUpdateRequest(request.pathVariable("id")) }
                .flatMap { bookService.update(it) }
                .then(ok().build())
    }

    private fun BookUpdateRequestDTO.toBookUpdateRequest(id: String) =
            BookUpdateRequest(
                    id = ObjectId(id),
                    title = title,
                    author = author,
                    isbnNumber = isbnNumber,
                    numberOfPages = numberOfPages,
                    rating = rating)

}