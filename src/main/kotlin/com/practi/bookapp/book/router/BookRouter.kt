package com.practi.bookapp.book.router

import com.practi.bookapp.book.handler.AddCommentHandler
import com.practi.bookapp.book.handler.CreateBookHandler
import com.practi.bookapp.book.handler.DeleteBookHandler
import com.practi.bookapp.book.handler.SearchBookHandler
import com.practi.bookapp.book.handler.UpdateBookHandler
import org.springframework.context.annotation.Bean
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.server.HandlerFunction
import org.springframework.web.reactive.function.server.RequestPredicates.DELETE
import org.springframework.web.reactive.function.server.RequestPredicates.GET
import org.springframework.web.reactive.function.server.RequestPredicates.POST
import org.springframework.web.reactive.function.server.RequestPredicates.PUT
import org.springframework.web.reactive.function.server.RouterFunction
import org.springframework.web.reactive.function.server.RouterFunctions.route
import org.springframework.web.reactive.function.server.ServerResponse

@Component
internal class BookRouter(
        private val createBookHandler: CreateBookHandler,
        private val searchBookHandler: SearchBookHandler,
        private val updateBookHandler: UpdateBookHandler,
        private val deleteBookHandler: DeleteBookHandler,
        private val addCommentHandler: AddCommentHandler
) {

    @Bean
    fun route(): RouterFunction<ServerResponse> = route(
            POST("/api/book"), HandlerFunction<ServerResponse>(createBookHandler::handle))
                .andRoute(GET("/api/book"), HandlerFunction<ServerResponse>(searchBookHandler::handle))
                .andRoute(PUT("/api/book/{id}"), HandlerFunction<ServerResponse>(updateBookHandler::handle))
                .andRoute(DELETE("/api/book/{id}"), HandlerFunction<ServerResponse>(deleteBookHandler::handle))
                .andRoute(POST("/api/book/{id}/comment"), HandlerFunction<ServerResponse>(addCommentHandler::handle))

}