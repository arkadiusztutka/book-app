package com.practi.bookapp.book.service

import com.mongodb.client.result.UpdateResult
import com.practi.bookapp.book.domain.Book
import com.practi.bookapp.book.domain.Comment
import com.practi.bookapp.book.repository.BookRepository
import com.practi.bookapp.book.request.AddCommentRequest
import com.practi.bookapp.book.request.BookCreateRequest
import com.practi.bookapp.book.request.BookSearchRequest
import com.practi.bookapp.book.request.BookUpdateRequest
import com.practi.bookapp.exception.BookIdNotFoundException
import io.mockk.every
import io.mockk.impl.annotations.InjectMockKs
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import io.mockk.slot
import io.mockk.verify
import org.assertj.core.api.Assertions.assertThat
import org.bson.types.ObjectId
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.data.domain.PageRequest
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@ExtendWith(MockKExtension::class)
internal class BookServiceImplTest {

    @MockK
    private lateinit var bookRepository: BookRepository

    @InjectMockKs
    private lateinit var bookService: BookServiceImpl

    @Test
    fun `should search books`() {
        // given
        every { bookRepository.findAllWithLastFiveComments(any()) } returns Flux.just(book)

        // when
        val results = bookService.search(searchRequest).toIterable().toList()

        // then
        val slot = slot<PageRequest>()
        verify { bookRepository.findAllWithLastFiveComments(capture(slot)) }

        assertThat(slot.captured).isEqualTo(PageRequest.of(searchRequest.page, searchRequest.limit))
        assertThat(results).containsExactly(book)
    }

    @Test
    fun `should create book`() {
        // given
        every { bookRepository.save(any<Book>()) } returns Mono.just(book)

        // when
        bookService.create(createRequest).block()

        // then
        val slot = slot<Book>()
        verify { bookRepository.save(capture(slot)) }

        assertThat(slot.captured).isEqualToIgnoringGivenFields(book, "id")
    }

    @Test
    fun `should update book`() {
        // given
        every { bookRepository.existsById(any<ObjectId>()) } returns Mono.just(true)
        every { bookRepository.save(any<Book>()) } returns Mono.just(book)

        // when
        bookService.update(updateRequest).block()

        // then
        val slot = slot<Book>()
        verify { bookRepository.existsById(bookId) }
        verify { bookRepository.save(capture(slot)) }

        assertThat(slot.captured).isEqualTo(book)
    }

    @Test
    fun `should not update book when id not found`() {
        // given
        every { bookRepository.existsById(any<ObjectId>()) } returns Mono.just(false)

        // when
        assertThrows<BookIdNotFoundException>("Given book id ${bookId.toHexString()} not exists") { bookService.update(updateRequest).block() }

        // then
        verify { bookRepository.existsById(bookId) }
    }

    @Test
    fun `should delete book`() {
        // given
        every { bookRepository.existsById(any<ObjectId>()) } returns Mono.just(true)
        every { bookRepository.deleteById(any<ObjectId>()) } returns Mono.empty()

        // when
        bookService.delete(bookId).block()

        // then
        verify { bookRepository.existsById(eq(bookId)) }
        verify { bookRepository.deleteById(eq(bookId)) }
    }

    @Test
    fun `should not delete book when book id not found`() {
        // given
        every { bookRepository.existsById(any<ObjectId>()) } returns Mono.just(false)

        // when
        assertThrows<BookIdNotFoundException>("Given book id ${bookId.toHexString()} not exists") { bookService.delete(bookId).block() }

        // then
        verify { bookRepository.existsById(eq(bookId)) }
    }

    @Test
    fun `should add comment to book`() {
        // given
        every { bookRepository.addCommentToBook(any(), any()) } returns Mono.just(UpdateResult.acknowledged(1, 1, null))

        // when
        bookService.addComment(addCommentRequest).block()

        // then
        val slot = slot<Comment>()
        verify { bookRepository.addCommentToBook(eq(bookId), capture(slot)) }

        assertThat(slot.captured).isEqualTo(Comment(commentString))
    }

    @Test
    fun `should not add comment to book when id not found`() {
        // given
        every { bookRepository.addCommentToBook(any(), any()) } returns Mono.just(UpdateResult.acknowledged(0, null, null))

        // when
        assertThrows<BookIdNotFoundException>("Given book id ${bookId.toHexString()} not exists") { bookService.addComment(addCommentRequest).block() }

        // then
        val slot = slot<Comment>()
        verify { bookRepository.addCommentToBook(eq(bookId), capture(slot)) }

        assertThat(slot.captured).isEqualTo(Comment(commentString))
    }

    companion object Fixtures {
        private val title = "title"
        private val bookId = ObjectId("5ea1ee0f85f1fb4510560cad")
        private val commentString = "comment"
        private val searchRequest = BookSearchRequest(0, 100)
        private val createRequest = BookCreateRequest(
                title = title,
                isbnNumber = "0000000000",
                author = "author",
                numberOfPages = 360,
                rating = 5
        )
        private val updateRequest = BookUpdateRequest(
                id = bookId,
                title = title,
                isbnNumber = "0000000000",
                author = "author",
                numberOfPages = 360,
                rating = 5)
        private val addCommentRequest = AddCommentRequest(bookId, commentString)
        private val book = Book(
                id = bookId,
                title = title,
                isbnNumber = "0000000000",
                author = "author",
                numberOfPages = 360,
                rating = 5)
    }
}