package com.practi.bookapp.book.repository

import com.practi.bookapp.book.domain.Book
import com.practi.bookapp.book.domain.Comment
import org.assertj.core.api.Assertions.assertThat
import org.bson.types.ObjectId
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest
import org.springframework.data.domain.PageRequest
import org.springframework.test.context.junit.jupiter.SpringExtension

@ExtendWith(SpringExtension::class)
@DataMongoTest
internal class BookRepositoryTest {

    @field:Autowired
    private lateinit var bookRepository: BookRepository

    @BeforeEach
    fun beforeEach() {
        bookRepository.saveAll(listOf(book1, book2, book3)).blockLast()
    }

    @AfterEach
    fun afterEach() {
        bookRepository.deleteAll().block()
    }

    @Test
    fun `should return all books with last five comments`() {
        // when
        val results = bookRepository.findAllWithLastFiveComments(PageRequest.of(0, 100)).toIterable().toList()

        //then
        assertThat(results).containsExactly(book1, book2, book3.copy(comments = comments))
    }

    @Test
    fun `should add comment to book`() {
        // when
        val results = bookRepository.addCommentToBook(bookId1, comment).block()

        //then
        assertThat(results).isNotNull
        assertThat(results!!.modifiedCount).isEqualTo(1)

        val updated = bookRepository.findById(bookId1).block()!!
        assertThat(updated.comments).hasSize(1)
        assertThat(updated.comments[0]).isEqualTo(comment)
    }

    companion object Fixtures {
        private val comment = Comment("comment")
        private val comments = mutableListOf(comment.copy("test1"),
                comment.copy("test2"), comment.copy("test3"), comment.copy("test4"),
                comment.copy("test5"))
        private val bookId1 = ObjectId("5ea1ee0f85f1fb4510560cad")
        private val bookId2 = ObjectId("5ea2a8a3c50cb363d6310cef")
        private val bookId3 = ObjectId("5ea2a8a3c50cb363d6310cf0")

        private val book1 = Book(id = bookId1, title = "title1")
        private val book2 = Book(id = bookId2, title = "title2", comments = listOf(comment))
        private val book3 = Book(id = bookId3, title = "title3", comments = listOf(comment) + comments)
    }

}