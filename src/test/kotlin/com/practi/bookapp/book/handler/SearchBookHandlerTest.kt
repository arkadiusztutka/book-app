package com.practi.bookapp.book.handler

import com.ninjasquad.springmockk.MockkBean
import com.practi.bookapp.book.domain.Book
import com.practi.bookapp.book.repository.BookRepository
import com.practi.bookapp.book.router.BookRouter
import com.practi.bookapp.book.service.BookServiceImpl
import io.mockk.every
import io.mockk.verify
import org.bson.types.ObjectId
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.Import
import org.springframework.data.domain.PageRequest
import org.springframework.http.MediaType
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.reactive.server.WebTestClient
import reactor.core.publisher.Flux

@ExtendWith(SpringExtension::class)
@ContextConfiguration(classes = [BookRouter::class, AddCommentHandler::class, CreateBookHandler::class,
    DeleteBookHandler::class, SearchBookHandler::class, UpdateBookHandler::class])
@Import(BookServiceImpl::class)
@WebFluxTest
internal class SearchBookHandlerTest {

    @MockkBean
    private lateinit var bookRepository: BookRepository

    @field:Autowired
    private lateinit var applicationContext: ApplicationContext

    private lateinit var webTestClient: WebTestClient

    @BeforeEach
    fun beforeEach() {
        webTestClient = WebTestClient.bindToApplicationContext(applicationContext).build()
    }

    @Test
    fun `should search books`() {
        // given
        every { bookRepository.findAllWithLastFiveComments(any()) } returns Flux.just(book)

        // when
        webTestClient.get()
                .uri {
                    it.path("/api/book")
                            .queryParam("page", page)
                            .queryParam("limit", limit)
                            .build()
                }
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk

        // then
        verify { bookRepository.findAllWithLastFiveComments(PageRequest.of(page, limit)) }
    }

    companion object Fixture {
        private const val bookId = "5ea1ee0f85f1fb4510560cad"
        private const val page = 0
        private const val limit = 100
        private val book = Book(
                id = ObjectId(bookId),
                title = "title",
                author = "author",
                isbnNumber = "1111111111",
                numberOfPages = 100,
                rating = 5
        )
    }
}