package com.practi.bookapp.book.handler

import com.ninjasquad.springmockk.MockkBean
import com.practi.bookapp.book.repository.BookRepository
import com.practi.bookapp.book.router.BookRouter
import com.practi.bookapp.book.service.BookServiceImpl
import com.practi.bookapp.exception.handler.GlobalErrorWebExceptionHandler
import io.mockk.every
import io.mockk.verify
import org.bson.types.ObjectId
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.Import
import org.springframework.http.MediaType
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.reactive.server.WebTestClient
import reactor.core.publisher.Mono

@ExtendWith(SpringExtension::class)
@ContextConfiguration(classes = [BookRouter::class, AddCommentHandler::class, CreateBookHandler::class,
    DeleteBookHandler::class, SearchBookHandler::class, UpdateBookHandler::class])
@Import(value = [BookServiceImpl::class, GlobalErrorWebExceptionHandler::class])
@WebFluxTest
internal class DeleteBookHandlerTest {

    @MockkBean
    private lateinit var bookRepository: BookRepository

    @field:Autowired
    private lateinit var applicationContext: ApplicationContext

    private lateinit var webTestClient: WebTestClient

    @BeforeEach
    fun beforeEach() {
        webTestClient = WebTestClient.bindToApplicationContext(applicationContext).build()
    }

    @Test
    fun `should delete book`() {
        // given
        every { bookRepository.existsById(any<ObjectId>()) } returns Mono.just(true)
        every { bookRepository.deleteById(any<ObjectId>()) } returns Mono.empty<Void>()

        // when
        webTestClient.delete()
                .uri("/api/book/{id}", bookId)
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk

        // then
        verify { bookRepository.existsById(eq(ObjectId(bookId))) }
        verify { bookRepository.deleteById(eq(ObjectId(bookId))) }
    }

    @Test
    fun `should not delete book when book id not found`() {
        // given
        every { bookRepository.existsById(any<ObjectId>()) } returns Mono.just(false)

        // when
        webTestClient.delete()
                .uri("/api/book/{id}", bookId)
                .exchange()
                .expectStatus().isBadRequest
                .expectBody()
                .json("{\"message\":\"Given book with id $bookId not exists\"}")

        // then
        verify { bookRepository.existsById(eq(ObjectId(bookId))) }
    }

    companion object Fixture {
        private const val bookId = "5ea1ee0f85f1fb4510560cad"
    }
}