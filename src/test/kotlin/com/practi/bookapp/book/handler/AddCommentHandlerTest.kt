package com.practi.bookapp.book.handler

import com.mongodb.client.result.UpdateResult
import com.ninjasquad.springmockk.MockkBean
import com.practi.bookapp.book.domain.Comment
import com.practi.bookapp.book.repository.BookRepository
import com.practi.bookapp.book.request.dto.AddCommentRequestDTO
import com.practi.bookapp.book.router.BookRouter
import com.practi.bookapp.book.service.BookServiceImpl
import com.practi.bookapp.exception.handler.GlobalErrorWebExceptionHandler
import io.mockk.every
import io.mockk.verify
import org.bson.types.ObjectId
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.Import
import org.springframework.http.MediaType.APPLICATION_JSON
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.reactive.server.WebTestClient
import reactor.core.publisher.Mono

@ExtendWith(SpringExtension::class)
@ContextConfiguration(classes = [BookRouter::class, AddCommentHandler::class, CreateBookHandler::class,
    DeleteBookHandler::class, SearchBookHandler::class, UpdateBookHandler::class])
@Import(value = [BookServiceImpl::class, GlobalErrorWebExceptionHandler::class])
@WebFluxTest
internal class AddCommentHandlerTest {

    @MockkBean
    private lateinit var bookRepository: BookRepository

    @field:Autowired
    private lateinit var applicationContext: ApplicationContext

    private lateinit var webTestClient: WebTestClient

    @BeforeEach
    fun beforeEach() {
        webTestClient = WebTestClient.bindToApplicationContext(applicationContext).build()
    }

    @Test
    fun `should add comment`() {
        // given
        every { bookRepository.addCommentToBook(any(), any()) } returns Mono.just(UpdateResult.acknowledged(1, null, null))

        // when
        webTestClient.post()
                .uri("/api/book/{id}/comment", bookId)
                .accept(APPLICATION_JSON)
                .body(Mono.just(AddCommentRequestDTO(comment)), AddCommentRequestDTO::class.java)
                .exchange()
                .expectStatus().isOk

        // then
        verify { bookRepository.addCommentToBook(ObjectId(bookId), Comment(comment)) }

    }

    @Test
    fun `should return bad request when comment is blank`() {
        // when
        webTestClient.post()
                .uri("/api/book/{id}/comment", bookId)
                .accept(APPLICATION_JSON)
                .body(Mono.just(AddCommentRequestDTO("")), AddCommentRequestDTO::class.java)
                .exchange()
                .expectStatus().isBadRequest
                .expectBody()
                .json("{\"errors\":[{\"message\":\"Comment cannot be blank\"}]}")

    }

    @Test
    fun `should return bad request when id is incorrect`() {
        // when
        webTestClient.post()
                .uri("/api/book/{id}/comment", "aaa")
                .accept(APPLICATION_JSON)
                .body(Mono.just(AddCommentRequestDTO(comment)), AddCommentRequestDTO::class.java)
                .exchange()
                .expectStatus().isBadRequest
                .expectBody()
                .json("{\"message\": \"Invalid book id: aaa\"}")
    }

    @Test
    fun `should return bad request when id not found`() {
        // given
        every { bookRepository.addCommentToBook(any(), any()) } returns Mono.just(UpdateResult.acknowledged(0, null, null))

        // when
        webTestClient.post()
                .uri("/api/book/{id}/comment", bookId)
                .accept(APPLICATION_JSON)
                .body(Mono.just(AddCommentRequestDTO(comment)), AddCommentRequestDTO::class.java)
                .exchange()
                .expectStatus().isBadRequest
                .expectBody()
                .json("{\"message\":\"Given book with id $bookId not exists\"}")

        // then
        verify { bookRepository.addCommentToBook(ObjectId(bookId), Comment(comment)) }
    }

    companion object Fixtures {
        private const val bookId = "5ea1ee0f85f1fb4510560cad"
        private const val comment = "custom comment"
    }
}