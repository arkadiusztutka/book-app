package com.practi.bookapp.book.handler

import com.ninjasquad.springmockk.MockkBean
import com.practi.bookapp.book.domain.Book
import com.practi.bookapp.book.repository.BookRepository
import com.practi.bookapp.book.request.dto.BookCreateRequestDTO
import com.practi.bookapp.book.router.BookRouter
import com.practi.bookapp.book.service.BookServiceImpl
import com.practi.bookapp.exception.handler.GlobalErrorWebExceptionHandler
import io.mockk.every
import io.mockk.slot
import io.mockk.verify
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.Import
import org.springframework.http.MediaType
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.reactive.server.WebTestClient
import reactor.core.publisher.Mono

@ExtendWith(SpringExtension::class)
@ContextConfiguration(classes = [BookRouter::class, AddCommentHandler::class, CreateBookHandler::class,
    DeleteBookHandler::class, SearchBookHandler::class, UpdateBookHandler::class])
@Import(value = [BookServiceImpl::class, GlobalErrorWebExceptionHandler::class])
@WebFluxTest
internal class CreateBookHandlerTest {

    @MockkBean
    private lateinit var bookRepository: BookRepository

    @field:Autowired
    private lateinit var applicationContext: ApplicationContext

    private lateinit var webTestClient: WebTestClient

    @BeforeEach
    fun beforeEach() {
        webTestClient = WebTestClient.bindToApplicationContext(applicationContext).build()
    }

    @Test
    fun `should create book with all values`() {
        // given
        every { bookRepository.save(any<Book>()) } returns Mono.empty<Book>()

        // when
        webTestClient.post()
                .uri("/api/book")
                .accept(MediaType.APPLICATION_JSON)
                .bodyValue(createRequest)
                .exchange()
                .expectStatus().isOk

        // then
        val slot = slot<Book>()
        verify { bookRepository.save(capture(slot)) }

        assertThat(slot.captured).isEqualToIgnoringGivenFields(book, "id")
    }

    @Test
    fun `should create book with minimal required values`() {
        // given
        every { bookRepository.save(any<Book>()) } returns Mono.empty<Book>()

        // when
        webTestClient.post()
                .uri("/api/book")
                .accept(MediaType.APPLICATION_JSON)
                .bodyValue(minimalCreateRequest)
                .exchange()
                .expectStatus().isOk

        // then
        val slot = slot<Book>()
        verify { bookRepository.save(capture(slot)) }

        assertThat(slot.captured).isEqualToIgnoringGivenFields(minimalBook, "id")
    }

    @Test
    fun `should not create book when title is blank`() {
        // when
        webTestClient.post()
                .uri("/api/book")
                .accept(MediaType.APPLICATION_JSON)
                .bodyValue(createRequest.copy(title = ""))
                .exchange()
                .expectStatus().isBadRequest
                .expectBody()
                .json("{\"errors\":[{\"message\":\"Title cannot be blank or null\"}]}")

    }

    companion object Fixture {
        private const val bookId = "5ea1ee0f85f1fb4510560cad"
        private val createRequest = BookCreateRequestDTO(
                title = "title",
                author = "author",
                isbnNumber = "1111111111",
                numberOfPages = 100,
                rating = 5
        )
        private val book = Book(
                title = createRequest.title,
                author = createRequest.author,
                isbnNumber = createRequest.isbnNumber,
                numberOfPages = createRequest.numberOfPages,
                rating = createRequest.rating
        )
        private val minimalCreateRequest = createRequest.copy(
                author = null,
                isbnNumber = null,
                numberOfPages = null,
                rating = null
        )
        private val minimalBook = book.copy(
                author = minimalCreateRequest.author,
                isbnNumber = minimalCreateRequest.isbnNumber,
                numberOfPages = minimalCreateRequest.numberOfPages,
                rating = minimalCreateRequest.rating
        )
    }
}