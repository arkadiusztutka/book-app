package com.practi.bookapp.book.handler

import com.ninjasquad.springmockk.MockkBean
import com.practi.bookapp.book.domain.Book
import com.practi.bookapp.book.repository.BookRepository
import com.practi.bookapp.book.request.dto.BookUpdateRequestDTO
import com.practi.bookapp.book.router.BookRouter
import com.practi.bookapp.book.service.BookServiceImpl
import com.practi.bookapp.exception.handler.GlobalErrorWebExceptionHandler
import io.mockk.every
import io.mockk.slot
import io.mockk.verify
import org.assertj.core.api.Assertions
import org.bson.types.ObjectId
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.Import
import org.springframework.http.MediaType
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.reactive.server.WebTestClient
import reactor.core.publisher.Mono

@ExtendWith(SpringExtension::class)
@ContextConfiguration(classes = [BookRouter::class, AddCommentHandler::class, CreateBookHandler::class,
    DeleteBookHandler::class, SearchBookHandler::class, UpdateBookHandler::class])
@Import(value = [BookServiceImpl::class, GlobalErrorWebExceptionHandler::class])
@WebFluxTest
internal class UpdateBookHandlerTest {

    @MockkBean
    private lateinit var bookRepository: BookRepository

    @field:Autowired
    private lateinit var applicationContext: ApplicationContext

    private lateinit var webTestClient: WebTestClient

    @BeforeEach
    fun beforeEach() {
        webTestClient = WebTestClient.bindToApplicationContext(applicationContext).build()
    }

    @Test
    fun `should update book with all values`() {
        // given
        every { bookRepository.existsById(any<ObjectId>()) } returns Mono.just(true)
        every { bookRepository.save(any<Book>()) } returns Mono.empty<Book>()

        // when
        webTestClient.put()
                .uri("/api/book/{id}", bookId)
                .accept(MediaType.APPLICATION_JSON)
                .bodyValue(updateRequest)
                .exchange()
                .expectStatus().isOk

        // then
        val slot = slot<Book>()
        verify { bookRepository.existsById(eq(ObjectId(bookId))) }
        verify { bookRepository.save(capture(slot)) }

        Assertions.assertThat(slot.captured).isEqualTo(book)
    }

    @Test
    fun `should update book with minimal required values`() {
        // given
        every { bookRepository.existsById(any<ObjectId>()) } returns Mono.just(true)
        every { bookRepository.save(any<Book>()) } returns Mono.empty<Book>()

        // when
        webTestClient.put()
                .uri("/api/book/{id}", bookId)
                .accept(MediaType.APPLICATION_JSON)
                .bodyValue(minimalUpdateRequest)
                .exchange()
                .expectStatus().isOk

        // then
        val slot = slot<Book>()
        verify { bookRepository.existsById(eq(ObjectId(bookId))) }
        verify { bookRepository.save(capture(slot)) }

        Assertions.assertThat(slot.captured).isEqualTo(minimalBook)
    }

    @Test
    fun `should not update book when book id not found`() {
        // given
        every { bookRepository.existsById(any<ObjectId>()) } returns Mono.just(false)

        // when
        webTestClient.put()
                .uri("/api/book/{id}", bookId)
                .accept(MediaType.APPLICATION_JSON)
                .bodyValue(minimalUpdateRequest)
                .exchange()
                .expectStatus().isBadRequest
                .expectBody()
                .json("{\"message\":\"Given book with id $bookId not exists\"}")

        // then
        verify { bookRepository.existsById(eq(ObjectId(bookId))) }
    }

    companion object Fixture {
        private const val bookId = "5ea1ee0f85f1fb4510560cad"
        private val updateRequest = BookUpdateRequestDTO(
                title = "title",
                author = "author",
                isbnNumber = "1111111111",
                numberOfPages = 100,
                rating = 5
        )
        private val book = Book(
                id = ObjectId(bookId),
                title = updateRequest.title,
                author = updateRequest.author,
                isbnNumber = updateRequest.isbnNumber,
                numberOfPages = updateRequest.numberOfPages,
                rating = updateRequest.rating
        )
        private val minimalUpdateRequest = updateRequest.copy(
                author = null,
                isbnNumber = null,
                numberOfPages = null,
                rating = null
        )
        private val minimalBook = book.copy(
                id = ObjectId(bookId),
                author = minimalUpdateRequest.author,
                isbnNumber = minimalUpdateRequest.isbnNumber,
                numberOfPages = minimalUpdateRequest.numberOfPages,
                rating = minimalUpdateRequest.rating
        )
    }
}