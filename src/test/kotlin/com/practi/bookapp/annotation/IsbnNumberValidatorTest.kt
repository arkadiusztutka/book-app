package com.practi.bookapp.annotation

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream

internal class IsbnNumberValidatorTest {

    private val validator = IsbnNumberValidator()

    @ParameterizedTest
    @MethodSource("isbnNumbers")
    fun `should test isbn numbers`(isbnNumber: String?, expected: Boolean) {
        // when
        val result = validator.isValid(isbnNumber, null)

        // then
        assertThat(result).isEqualTo(expected)
    }

    companion object Fixtures {

        @JvmStatic
        private fun isbnNumbers() = Stream.of(
                Arguments.of(null, true),
                Arguments.of("0306406152", true),
                Arguments.of("9788371815102", true),
                Arguments.of("123456789a", false),
                Arguments.of("11111", false),
                Arguments.of("0-306-40615-2", false),
                Arguments.of("978-83-7181-510-2", false),
                Arguments.of("0111111116", false),
                Arguments.of("2222222222229", false),
                Arguments.of("ISBN-10: 0-306-40615", false),
                Arguments.of("ISBN-13: 978-83-7181-510-2", false),
                Arguments.of("ISBN-10: 030640615", false),
                Arguments.of("ISBN-13: 9788371815102", false),
                Arguments.of("ISBN-10 0-306-40615", false),
                Arguments.of("ISBN-13 978-83-7181-510-2", false),
                Arguments.of("ISBN-10 030640615", false),
                Arguments.of("ISBN-13 9788371815102", false)
        )
    }
}